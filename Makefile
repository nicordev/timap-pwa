include .env
include .env.local
export

.PHONY: update-build
update-build:
	@if [ ! -d "${TIMAP_DIRECTORY}" ]; then echo "missing TIMAP_DIRECTORY"; return 1; fi; \
	if [ ! -d "${TIMAP_DIRECTORY}/dist/pwa" ]; then echo "missing dist/pwa directory in ${TIMAP_DIRECTORY}"; return 1; fi; \
	rm -rf pwa; \
    cp -r "${TIMAP_DIRECTORY}/dist/pwa" .; \
	echo "pwa updated";

.PHONY: push
push:
	git add . && git commit -m 'update pwa' && git push origin master
